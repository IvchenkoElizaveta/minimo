var images = document.querySelectorAll('img');
var modalImg = document.querySelector("#img01");
var modal = document.querySelector("#myModal");
var span = document.querySelector(".close");

for (i = 0; i < images.length; ++i) {
	let image = images[i];
	image.onclick = function(){
		// перевели popup из display = none в block
		modal.style.display = "block";
		// берем сслыку картинки по которой кликнули 
		// и присваиваем ее "картинке" в popup
		modalImg.src = this.src;
	}
}
// закрыли popup при клике на крестик
span.onclick = function() { 
	modal.style.display = "none";
}